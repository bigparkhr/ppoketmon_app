import 'package:ppoketmon_app/model/ppoketmon_item.dart';

class PpoketmonDetailResult {
  String msg;
  num id;
  PpoketmonItem data;

  PpoketmonDetailResult(this.msg, this.id, this.data);

  factory PpoketmonDetailResult.fromJson(Map<String, dynamic> json) {
    return PpoketmonDetailResult(
      json['msg'],
      json['id'],
      PpoketmonItem.fromJson(json['data']),
    );
  }
}