class PpoketmonItem {
  num id;
  String pocketImg;
  String pocketName;
  String pocketType;
  num hp;
  num speed;
  num offensive;
  num defensive;
  num evolutionGrade;
  String etcMemo;

  PpoketmonItem(this.id, this.pocketImg, this.pocketName, this.pocketType, this.hp, this.speed, this.offensive, this.defensive, this.evolutionGrade, this.etcMemo);

  factory PpoketmonItem.fromJson(Map<String, dynamic> json) {
    return PpoketmonItem(
        json['id'],
        json['pocketImg'],
        json['pocketName'],
        json['pocketType'],
        json['hp'],
        json['speed'],
        json['offensive'],
        json['defensive'],
        json['evolutionGrade'],
        json['etcMemo'],
    );
  }
}