import 'package:ppoketmon_app/model/ppoketmon_item.dart';

class PpoketmonListResult {
  String msg;
  num code;
  List<PpoketmonItem> lsit;
  num totalCount;

  PpoketmonListResult(this.msg, this.code, this.lsit, this.totalCount);

  factory PpoketmonListResult.fromJson(Map<String, dynamic> json) {
    return PpoketmonListResult(
        json['msg'],
        json['code'],
        json['list'] != null ?
        (json['list'] as List).map((e) => PpoketmonItem.fromJson(e)).toList() : [],
        json['totalCount']
    );
  }
}
