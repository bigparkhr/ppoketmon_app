class PpoketmonResponse {
  num id;
  String pocketImg;
  String pocketName;
  String pocketType;

  PpoketmonResponse(this.id, this.pocketImg, this.pocketName, this.pocketType);

  factory PpoketmonResponse.fromJson(Map<String, dynamic> json)=> PpoketmonResponse(
        json['id'],
        json['pocketImg'],
        json['pocketName'],
        json['pocketType']
    );
}