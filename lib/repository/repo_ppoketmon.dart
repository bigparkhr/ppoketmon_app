import 'package:dio/dio.dart';
import 'package:ppoketmon_app/config/config_api.dart';
import 'package:ppoketmon_app/model/ppoektmon_detail_result.dart';
import 'package:ppoketmon_app/model/ppoketmon_list_result.dart';


class RepoPpoketmon {
  Future<PpoketmonListResult> getPpoketmons() async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/v1/ppoketmon/all';

    final response = await dio.get(
        _baseUrl,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            print(status);
            return status == 200;
          }
       )
    );
    return PpoketmonListResult.fromJson(response.data);
  }

  Future<PpoketmonDetailResult> getPpoketmon(num id) async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/v1/fight-stage/new/fight-id/{id}';
    
    final response = await dio.get(
      _baseUrl.replaceAll('{id}', id.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    return PpoketmonDetailResult.fromJson(response.data);
  }
}