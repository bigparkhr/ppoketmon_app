import 'package:flutter/material.dart';
import 'package:ppoketmon_app/model/ppoketmon_item.dart';

class ComponentsStandByBattle extends StatefulWidget {
  const ComponentsStandByBattle({
    super.key,
    required this.ppoketmonItem,
    required this.callback,
    required this.isMyTurn,
    required this.isLive,
    required this.currentHp,
  });

  final PpoketmonItem ppoketmonItem;
  final VoidCallback callback;
  final bool isMyTurn;
  final bool isLive;
  final num currentHp;

  @override
  State<ComponentsStandByBattle> createState() => _ComponentsStandByBattleState();
}

class _ComponentsStandByBattleState extends State<ComponentsStandByBattle> {
  double _calculateHpPercent() {
    return widget.currentHp / widget.ppoketmonItem.hp;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width / 3,
            height: MediaQuery.of(context).size.width / 3,
            child: Image.asset(
                '${widget.isLive ? widget.ppoketmonItem.pocketImg : 'assets/ppo1.png'}',
            fit: BoxFit.fill,
            ),
          ),
          Text(widget.ppoketmonItem.pocketName),
          Text('총 HP ${widget.ppoketmonItem.hp}'),
          Text('스피드 ${widget.ppoketmonItem.speed}'),
          Text('공격력 ${widget.ppoketmonItem.offensive}'),
          Text('방어력 ${widget.ppoketmonItem.defensive}'),
          SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: LinearProgressIndicator(
              value: _calculateHpPercent(),
            ),
          ),
          (widget.isMyTurn && widget.isLive) ?
          Container( width: 50, height: 35,
            child: OutlinedButton(
              onPressed: widget.callback,
              child: Text('공격',style: TextStyle(color: Colors.black),),
            ),
          ) : Container(width: 50, height: 35,),
        ],
      ),
    );
  }
}
