import 'package:flutter/material.dart';
import 'package:ppoketmon_app/model/ppoketmon_item.dart';

class ComponentsPpoketmonItem extends StatelessWidget {
  const ComponentsPpoketmonItem({
    super.key,
    required this.ppoketmonitem,
    required this.callback,
  });

  final PpoketmonItem ppoketmonitem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Image.asset('${ppoketmonitem.pocketImg}'),
            Container(
              child: Row(
                children: [
                  Text('${ppoketmonitem.pocketType}'),
                  Text('${ppoketmonitem.pocketType}'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
