import 'package:flutter/material.dart';
import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:ppoketmon_app/components/components_ppoketmon_item.dart';
import 'package:ppoketmon_app/model/ppoketmon_item.dart';
import 'package:ppoketmon_app/pages/page_ppoketmon_detail.dart';
import 'package:ppoketmon_app/repository/repo_ppoketmon.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key,});

  @override
  State<PageIndex> createState() => _PageIndexState();
}


class _PageIndexState extends State<PageIndex> {

  List<PpoketmonItem> _list = [];

  Future<void> _loadData() async {
    await RepoPpoketmon().getPpoketmons()
        .then((res) => {
      setState(() {
        _list = res.lsit;
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  String searchValue = '';
  final List<String> _suggestions = ['Afeganistan', 'Albania', 'Algeria', 'Australia', 'Brazil', 'German', 'Madagascar', 'Mozambique', 'Portugal', 'Zambia'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EasySearchBar(
          title: const Text('포켓몬 도감'),
          backgroundColor: Colors.black45,
          titleTextStyle: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.white60
          ),
          actions: [
            IconButton(onPressed: () {}, icon: Icon(Icons.more_vert)),
          ],
          iconTheme: IconThemeData(color: Colors.white),
          onSearch: (value) => setState(() => searchValue = value),
          suggestions: _suggestions
      ),
      drawer: Drawer(
        child: ListView(
            padding: EdgeInsets.zero,
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.black45,
                ),
                child: Text('Ppoketmon'),
              ),
              ListTile(
                  title: const Text('Ring'),
                  onTap: () => Navigator.pop(context)
              ),
              ListTile(
                  title: const Text('Necklace'),
                  onTap: () => Navigator.pop(context)
              )
            ]
        ),
      ),
      body: _buildbody(context),
    );
  }


  Widget _buildbody(BuildContext context) {
    return SingleChildScrollView(
      child: GridView.builder(
        physics: ScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
            mainAxisSpacing: 5,
            crossAxisSpacing: 5
        ),
        itemCount: _list.length,
        itemBuilder: (BuildContext context, int index) {
          return ComponentsPpoketmonItem(
              ppoketmonitem: _list[index],
              callback: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PagePpoketmonDetail(ppoketmonitem: _list[index], id: _list[index].id)));
              });
        },
        ),
    );
  }
}
