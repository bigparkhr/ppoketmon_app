import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:ppoketmon_app/components/components_stand_by_battle.dart';
import 'package:ppoketmon_app/model/ppoketmon_item.dart';

class PageStandByBattle extends StatefulWidget {
  const PageStandByBattle({
    super.key
  });

  @override
  State<PageStandByBattle> createState() => _PageStandByBattleState();
}

class _PageStandByBattleState extends State<PageStandByBattle> {
  PpoketmonItem ppoketmon1 = PpoketmonItem(1, 'assets/ppo1.png', '이상해씨', '풀타입', 45, 45, 49, 49, 1, '자신의 체력이 1/3이하만 남으면, 자신이 사용하는 풀 타입 기술의 위력이 50%증가한다');
  PpoketmonItem ppoketmon2 = PpoketmonItem(2, 'assets/ppo2.png', '이상해풀', '풀타입', 60, 60, 62, 63, 2, '자신의 체력이 1/3이하만 남으면, 자신이 사용하는 풀 타입 기술의 위력이 50%증가한다');

  bool _isTrunPpoektmon1 = true;

  bool _ppoketmon1Live = true;
  num _ppoketmon1CurrentHp = 0;

  bool _ppoketmon2Live = true;
  num _ppoketmon2CurrentHp = 0;

  String _gamelog = '';

  String searchValue = '';
  final List<String> _suggestions = ['Afeganistan', 'Albania', 'Algeria', 'Australia', 'Brazil', 'German', 'Madagascar', 'Mozambique', 'Portugal', 'Zambia'];

  @override
  void initState() {
    super.initState();
    _calculateFirstTurn();
    setState(() {
      _ppoketmon1CurrentHp = ppoketmon1.hp;
      _ppoketmon2CurrentHp = ppoketmon2.hp;
    });
  }

  void _calculateFirstTurn() {
    if (ppoketmon1.speed < ppoketmon2.speed) {
      setState(() {
        _isTrunPpoektmon1 = false;
      });
    }
  }

  num _calculateResultHitPoint(num myHitPower, num targetDefPower) {
    List<num> criticalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    criticalArr.shuffle();

    bool isCritical = false;
    if (criticalArr[0] == 1) isCritical = true;

    num resultHit = myHitPower;
    if (isCritical) resultHit = resultHit * 2;
    resultHit = resultHit - targetDefPower;
    resultHit = resultHit.round();

    if(resultHit <= 0) resultHit = 1;

    return resultHit;
  }

  void _checkIsDead(num targetMonster) {
    if (targetMonster == 1 && (_ppoketmon1CurrentHp <= 0)) {
      setState(() {
        _ppoketmon1Live = false;
      });
    } else if (targetMonster == 2 && (_ppoketmon2CurrentHp <= 0)) {
      setState(() {
        _ppoketmon2Live = false;
      });
    }
  }

  void _attMonster(num actionMonster) {
    num myHitPower = ppoketmon1.offensive;
    num targetDefPower = ppoketmon2.offensive;

    if (actionMonster == 2) {
      myHitPower = ppoketmon2.offensive;
      targetDefPower = ppoketmon1.offensive;
    }

    num resultHit = _calculateResultHitPoint(myHitPower, targetDefPower);

    if (actionMonster == 1) {
      setState(() {
        _ppoketmon2CurrentHp -= resultHit;
        if (_ppoketmon2CurrentHp <= 0) _ppoketmon2CurrentHp = 0;
        _checkIsDead(2);
      });
    } else {
      setState(() {
        _ppoketmon1CurrentHp -= resultHit;
        if (_ppoketmon1CurrentHp <= 0) _ppoketmon1CurrentHp = 0;
        _checkIsDead(1);
      });
    }

    setState(() {
      _isTrunPpoektmon1 = !_isTrunPpoektmon1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EasySearchBar(
          title: const Text('포켓몬 도감'),
          backgroundColor: Colors.green,
          titleTextStyle: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.white60
          ),
          actions: [
            IconButton(onPressed: () {}, icon: Icon(Icons.more_vert)),
          ],
          iconTheme: IconThemeData(color: Colors.white),
          onSearch: (value) => setState(() => searchValue = value),
          suggestions: _suggestions
      ),
      drawer: Drawer(
        child: ListView(
            padding: EdgeInsets.zero,
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.black45,
                ),
                child: Text('Ppoketmon'),
              ),
              ListTile(
                  title: const Text('Ring'),
                  onTap: () => Navigator.pop(context)
              ),
              ListTile(
                  title: const Text('Necklace'),
                  onTap: () => Navigator.pop(context)
              )
            ]
        ),
      ),
      body: _buildbody(context),
    );
  }

  Widget _buildbody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            Stack(
              children: [
                Image.asset('assets/battle.jpg'),
                Container( margin: EdgeInsets.fromLTRB(390, 0, 30, 30), width: 450, height: 450,
                  child: ComponentsStandByBattle(
                      ppoketmonItem: ppoketmon1,
                      callback: () {
                        _attMonster(1);
                      },
                      isMyTurn: _isTrunPpoektmon1,
                      isLive: _ppoketmon1Live,
                      currentHp: _ppoketmon1CurrentHp),
                ),
                Container( margin: EdgeInsets.fromLTRB(5, 160, 400, 0), width: 500, height: 500,
                  child: ComponentsStandByBattle(
                      ppoketmonItem: ppoketmon2,
                      callback: () {
                        _attMonster(2);
                      },
                      isMyTurn: !_isTrunPpoektmon1,
                      isLive: _ppoketmon2Live,
                      currentHp: _ppoketmon2CurrentHp
                  ),
                ),
                Text(_gamelog),
              ],
            )
          ],
        ),
      ),
    );
  }
}
