import 'package:flutter/material.dart';
import 'package:ppoketmon_app/model/ppoketmon_item.dart';
import 'package:ppoketmon_app/model/ppoketmon_response.dart';
import 'package:ppoketmon_app/repository/repo_ppoketmon.dart';

class PagePpoketmonDetail extends StatefulWidget {
  const PagePpoketmonDetail({
    super.key,
    required this.ppoketmonitem,
    required this.id,
  });

  final PpoketmonItem ppoketmonitem;
  final num id;

  @override
  State<PagePpoketmonDetail> createState() => _PagePpoketmonDetailState();
}

class _PagePpoketmonDetailState extends State<PagePpoketmonDetail> {
  PpoketmonItem? _detail;

  Future<void> _loadDetail() async {
    await RepoPpoketmon().getPpoketmon(widget.id)
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    if (_detail == null) {
      return Text('Data Loading...');
    } else{
      return SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Text('${widget.ppoketmonitem.id}'),
              Image.asset(widget.ppoketmonitem.pocketImg),
              Text(widget.ppoketmonitem.pocketName),
              Text(widget.ppoketmonitem.pocketType),
              Text('${widget.ppoketmonitem.hp}'),
              Text('${widget.ppoketmonitem.speed}'),
              Text('${widget.ppoketmonitem.offensive}'),
              Text('${widget.ppoketmonitem.defensive}'),
              Text('${widget.ppoketmonitem.etcMemo}'),
            ],
          ),
        ),
      );
    }
  }
}
